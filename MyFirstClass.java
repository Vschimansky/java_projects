public class MyFirstClass {
    
    public static void main(String[] args){
            String make = "Ford";
            String model = "F250";
            double engineSize = 3.0;
            byte gear = 3;
            short speed = (short)(gear * 20);
    
            System.out.println("The make is: " + make);
            System.out.println("The model is: " + model);
            System.out.println("The engine size is: " + engineSize);
            System.out.println("The speed is: " + speed);

            if(engineSize > 1.3){
                System.out.println("The car is powerful");
            }
            else System.out.println("The car is weak");
/*
            switch(engineSize){
                
                case(engineSize > 1.3): System.out.println("The car is powerful");
                                        break;
                case(engineSize <= 1.3): System.out.println("The car is weak");
                                        break;
            }
*/


            int leapYearCount = 0;
            for(int i = 1900; i < 2001; i++){
                if((i%4) == 0){
                    System.out.println(i);
                    leapYearCount++;
                }
                if(leapYearCount >= 5){
                    break;
                }
            }
    
        }    

}