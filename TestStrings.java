public class TestStrings {
    public static void main(String[] args) {

        // CHAPTER 10 PART 1.2
        /*
        String string1 = "example.doc";
        System.out.println(string1);
        string1 = string1.substring(0,8).concat("bak");
        System.out.println(string1);*/


        // initializing strings
        String string1 = "this is the end";
        String string2 = "this is the end";
        String string3 = "this is not the end";
        String string4 = "the quick brown fox swallowed down the lazy chicken";
        String string5 = "Live not on evil";

        // CHAPTER 10 PART 1.3
        // comparing strings
        /*if(string1.compareTo(string2) == 0) System.out.println("String1 and String2 are equal");
        if(string2.compareTo(string3) > 0){
            System.out.println(string2 + "\tthis string is greater");
        }
        else if(string2.compareTo(string3) < 0){
            System.out.println(string3 + "\tthis string is greater");
        }
        else System.out.println("The strings are equal");*/

        // CHAPTER 10 PART 1.4
        // finding number of occurences of a substring in a string
        /*String[] stringArray = string4.split("ow", -2);
        System.out.println("Total occurences: " + (stringArray.length - 1));
        */

        // CHAPTER 10 PART 1.5 
        // Tests whether string is a palindrome
        /*string5.toLowerCase();
        char[] arr = new char[string5.length()];
        for(int i = 0; i < string5.length(); i++){
            arr[i] = string5.charAt(i);
        }
        String string6 = String.valueOf(arr);
        if(string5.compareTo(string6) == 0){
            System.out.println("PALINDROME!!!!");
        }
        else System.out.println("Not a palindrome.");*/

        
   
    }
}