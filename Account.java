public abstract class Account {
    

    // Creating private members
    private double balance;
    private String name;
    private static double interestRate;

    // Sets the interest rate
    public static void setInterestRate(double rate){
        interestRate = rate;
    }

    // Gets the interest rate
    public static double getInterestRate(){
        return interestRate;
    }

    // Constructor with parameters
    public Account(String name, double balance){
        this.name = name;
        this.balance = balance;
    }

    // Constructor without parameters
    public Account(){
        this("Victoria", 50);
    }
    
    // Sets the balance
    void setBalance(double balance){
        this.balance = balance;
    }

    // Gets the balance
    double getBalance(){
        return balance;
    }

    // Sets the name
    void setName(String name){
        this.name = name;
    }

    // Gets the name
    String getName(){
        return name;
    }

    // Adds interest to the balance
    public abstract void addInterest();

    // Withdraws money and returns boolean if enough in balance
    public boolean withdraw(double amount){
        if(amount <= this.balance){
            this.balance = this.balance - amount;
            return true;
        }
        else {
            return false;
        }
    }

    // Withdraws $20 and returns boolean if enough in balance
    public boolean withdraw(){
        return(withdraw(20));
    }

}