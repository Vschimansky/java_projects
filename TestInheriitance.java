public class TestInheriitance {

    public static void main(String[] args) {
        
        // Creating and initializing array
        Account[] accounts = new Account[3];
        accounts[0] = new SavingsAccount("Victor", 2);
        accounts[1] = new SavingsAccount("Hugo", 4);
        accounts[2] = new CurrentAccount("June", 6);

        // Adding interest to the accounts
        for(int i = 0; i < accounts.length; i++){
            accounts[i].addInterest();
            //System.out.println(accounts[i].getBalance());
        }

    }

}